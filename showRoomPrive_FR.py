#!/usr/bin/python3.8
# -*- coding: utf-8 -*-
import importlib
import json
import re
import time
from numpy import disp
import openpyxl
from pikepdf import parse_content_stream
from regex import B
import xlsxwriter
from openpyxl import load_workbook
from selenium import webdriver
from selenium.webdriver.support.select import Select
from selenium.webdriver.common.by import By
LOGIN = 'CHANGE_ME@gmail.com'
PASSWORD = 'CHANGE_ME'
SCROLL_PAUSE_TIME = 4.5
ALL_PRODUCTS = "https://www.showroomprive.com/tousproduits.aspx?vente="
PRODUCT = "https://www.showroomprive.com/ficheproduit.aspx?produit="
BOUTIQUE = "https://www.showroomprive.com/boutique/produits.aspx?boutique="
XLSX_TEMPLATE = "files/template.xlsx"
JSON_VISITED_SHOPS = "files/visited_shops.json"
SHOPS = []


class ShowRoomPrive_FR():
    def __init__(self):
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--ignore-certificate-errors')
        chrome_options.add_argument("--test-type")
        chrome_options.add_argument("--start-maximized")
        chrome_options.add_argument("--incognito")
        chrome_options.binary_location = "/usr/bin/google-chrome"
        self.driver = webdriver.Chrome(options=chrome_options)
        self.driver.delete_all_cookies()
        self.timestr = time.strftime("%Y-%m-%d-%H:%M:%S")
        self.visited_shops_links = []

    def login(self):
        self.driver.get('https://www.showroomprive.com/')
        time.sleep(1.5)
        self.driver.find_element(by=By.ID, value='agree_button').click()
        time.sleep(1.5)
        password_area = self.driver.find_element(by=By.ID, value='lPassword')
        login_area = self.driver.find_element(by=By.ID, value="lLogin")
        login_area.send_keys(LOGIN)
        time.sleep(1)
        password_area.send_keys(PASSWORD)
        time.sleep(3)
        try:
            self.driver.find_element(
                by=By.XPATH, value="//*[text()='Connexion']").click()
        except Exception as e:
            print(e)
            self.driver.quit()
        time.sleep(2)

    def get_outlet_alloffer(self):
        timestr = time.strftime("%Y-%m-%d-%H:%M:%S")
        file = 'outlout_allOffer_'+timestr+'.html'
        f = open(file, 'w')
        f.write(f'<!DOCTYPE html><html lang="en"><head><title> {file}</title><meta charset="utf-8"><meta name="viewport" content="width=device-width, initial-scale=1"><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"><script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script><link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css"><script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script><script src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script></head><body><div class="container" style="width:60%" ;><table class="table table-striped table-bordered" id="sortTable"><thead><tr><th title="Field #1">Shop</th><th title="Field #2">Product</th><th title="Field #3">Final_price</th><th title="Field #4">Original_Price</th><th title="Field #5">Discount</th><th title="Field #6">Score</th><th title="Field #7">Image</th></tr></thead><tbody><tr>\n')
        self.driver.get(
            'https://www.showroomprive.com/boutique/produits.aspx?boutique=outlet_alloffer')
        time.sleep(1.5)
        self.get_products_detail(f)
        f.write(
            "<th>.</th><th>.</th><th>.</th><th>.</th><th>.</th><th>.</th><th>.</th></tr></tbody></table></div><script>$('#sortTable').DataTable();</script></body></html>\n")
        f.close()

    def get_outlet_80(self):
        timestr = time.strftime("%Y-%m-%d-%H:%M:%S")
        file = 'outlout_80_'+timestr+'.html'
        f = open(file, 'w')
        f.write(f'<!DOCTYPE html><html lang="en"><head><title> {file}</title><meta charset="utf-8"><meta name="viewport" content="width=device-width, initial-scale=1"><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"><script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script><link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css"><script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script><script src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script></head><body><div class="container" style="width:60%" ;><table class="table table-striped table-bordered" id="sortTable"><thead><tr><th title="Field #1">Shop</th><th title="Field #2">Product</th><th title="Field #3">Final_price</th><th title="Field #4">Original_Price</th><th title="Field #5">Discount</th><th title="Field #6">Score</th><th title="Field #7">Image</th></tr></thead><tbody><tr>\n')
        self.driver.get(
            'https://www.showroomprive.com/boutique/produits.aspx?boutique=outlet_80')
        time.sleep(1.5)
        self.get_products_detail(f)
        f.write(
            "<th>.</th><th>.</th><th>.</th><th>.</th><th>.</th><th>.</th><th>.</th></tr></tbody></table></div><script>$('#sortTable').DataTable();</script></body></html>\n")
        f.close()

    def get_specific_shops(self):
        # Shops = ["97009","96996","96991","96962","96940","96925","96893","96892","96881","96875","96854","96837","96834","96832","96809","96802","96800","96798","96785","96783","96781","96774","96773","96772","96759","96754","96751","96730","96729","96727","96720","96714","96712","96708","96707","96703","96700","96689","96687","96686","96684","96680","96674","96651","96648","96646","96637","96632","96630","96628","96616","96613","96608","96602","96599","96595","96584","96583","96579","96578","96574","96561","96557","96548","96546","96535","96531","96524","96523","96512","96506","96502","96498","96479","96478","96476","96473","96472","96459","96456","96452","96437","96430","96427","96426","96425","96424","96423","96422","96420","96406","96396","96394","96337","96335","96322","96284","96233","96230","96218","96035","95789","95779","95745","95597","90326","88574"]

        Shops = ["97330"]  # "97208","97172","97158","97105","97062","97052","97046","96979","96950","96944","96937","96930","96928","96926","96923","96917","96889","96882","96750","96726","96723","96704","96620","96436"]
        timestr = time.strftime("%Y-%m-%d-%H:%M:%S")
        file = 'specific_shops_'+timestr+'.html'
        f = open(file, 'w')
        f.write(f'<!DOCTYPE html><html lang="en"><head><title> {file}</title><meta charset="utf-8"><meta name="viewport" content="width=device-width, initial-scale=1"><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"><script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script><link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css"><script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script><script src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script></head><body><div class="container" style="width:60%" ;><table class="table table-striped table-bordered" id="sortTable"><thead><tr><th title="Field #1">Shop</th><th title="Field #2">Product</th><th title="Field #3">Final_price</th><th title="Field #4">Original_Price</th><th title="Field #5">Discount</th><th title="Field #6">Score</th><th title="Field #7">Image</th></tr></thead><tbody><tr>\n')
        for shop in Shops:
            self.driver.get(ALL_PRODUCTS + shop)
            time.sleep(1.5)
            self.get_product_detail(f)
        f.write(
            "<th>.</th><th>.</th><th>.</th><th>.</th><th>.</th><th>.</th><th>.</th></tr></tbody></table></div><script>$('#sortTable').DataTable();</script></body></html>\n")
        f.close()

    def get_permenant_shops(self):
        Shops = ["universbeaute_goldenhour_visage_corps", "universbeaute_goldenhour_parfum",
                 "universbeaute_goldenhour_maquillage", "universbeaute_goldenhour_electrobeaute", "universbeaute_goldenhour_capillaire"]
        timestr = time.strftime("%Y-%m-%d-%H:%M:%S")
        file = 'permenant_shops_'+timestr+'.html'
        f = open(file, 'w')
        f.write(f'<!DOCTYPE html><html lang="en"><head><title> {file}</title><meta charset="utf-8"><meta name="viewport" content="width=device-width, initial-scale=1"><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"><script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script><link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css"><script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script><script src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script></head><body><div class="container" style="width:60%" ;><table class="table table-striped table-bordered" id="sortTable"><thead><tr><th title="Field #1">Shop</th><th title="Field #2">Product</th><th title="Field #3">Final_price</th><th title="Field #4">Original_Price</th><th title="Field #5">Discount</th><th title="Field #6">Score</th><th title="Field #7">Image</th></tr></thead><tbody><tr>\n')
        for shop in Shops:
            self.driver.get(ALL_PRODUCTS + shop)
            time.sleep(1.5)
            self.get_product_detail(f)
        f.write(
            "<th>.</th><th>.</th><th>.</th><th>.</th><th>.</th><th>.</th><th>.</th></tr></tbody></table></div><script>$('#sortTable').DataTable();</script></body></html>\n")
        f.close()

    def get_all_mode_products(self):
        MODE_SHOPS = []
        timestr = time.strftime("%Y-%m-%d-%H:%M:%S")
        file = 'MODE_'+timestr+'.html'
        f = open(file, 'w')
        f.write(f'<!DOCTYPE html><html lang="en"><head><title> {file}</title><meta charset="utf-8"><meta name="viewport" content="width=device-width, initial-scale=1"><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"><script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script><link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css"><script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script><script src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script></head><body><div class="container" style="width:60%" ;><table class="table table-striped table-bordered" id="sortTable"><thead><tr><th title="Field #1">Shop</th><th title="Field #2">Product</th><th title="Field #3">Final_price</th><th title="Field #4">Original_Price</th><th title="Field #5">Discount</th><th title="Field #6">Score</th><th title="Field #7">Image</th></tr></thead><tbody><tr>\n')
        self.driver.get('https://www.showroomprive.com/univers-srp/mode?u=1')
        time.sleep(1.5)
        self.scroll_down()
        self.scroll_up()
        try:
            ventes = self.driver.find_elements(
                by=By.CLASS_NAME, value="sale-block.see-more")
            print(ventes.text)
            self.driver.quit()
        except Exception as e:
            print(e)

        for v in ventes:
            vente = v.get_attribute('data-saleid')
            if vente not in SHOPS and vente not in MODE_SHOPS:
                MODE_SHOPS.append(vente)
                SHOPS.append(vente)
        print(' +++++++++++ MODE_SHOPS :', MODE_SHOPS)
        for i in MODE_SHOPS:
            url = "https://www.showroomprive.com/vente.aspx?vente="+i
            print("  url", url)
            self.driver.get(url)
            self.get_products_detail(f)
            f.write(
                "<th>.</th><th>.</th><th>.</th><th>.</th><th>.</th><th>.</th><th>.</th></tr></tbody></table></div><script>$('#sortTable').DataTable();</script></body></html>\n")
        f.close()

    def get_score(self, discount, price):
        score = 0
        new_price = float(price)
        # discount
        if discount >= 95:
            score += 10
        elif discount >= 90:
            score += 9.5
        elif discount >= 85:
            score += 9
        elif discount >= 80:
            score += 8.5
        elif discount >= 75:
            score += 8
        elif discount >= 70:
            score += 7.5
        elif discount >= 65:
            score += 7
        elif discount >= 60:
            score += 6.5
        elif discount >= 55:
            score += 6
        elif discount >= 50:
            score += 5.5
        elif discount >= 45:
            score += 5
        elif discount >= 40:
            score += 4.5
        elif discount >= 35:
            score += 4
        elif discount >= 30:
            score += 3.5
        elif discount >= 25:
            score += 3
        elif discount >= 20:
            score += 2.5
        elif discount >= 15:
            score += 2
        elif discount >= 10:
            score += 1.5
        else:
            score += 1
        # new_price
        if new_price <= 5:
            score += 10
        elif new_price <= 10:
            score += 9.5
        elif new_price <= 15:
            score += 9
        elif new_price <= 20:
            score += 8.5
        elif new_price <= 25:
            score += 8
        elif new_price <= 30:
            score += 7.5
        elif new_price <= 35:
            score += 7
        elif new_price <= 40:
            score += 6.5
        elif new_price <= 45:
            score += 6
        elif new_price <= 50:
            score += 5.5
        elif new_price <= 55:
            score += 5
        elif new_price <= 60:
            score += 4.5
        elif new_price <= 65:
            score += 4
        elif new_price <= 70:
            score += 3.5
        elif new_price <= 75:
            score += 3
        elif new_price <= 80:
            score += 2.5
        elif new_price <= 85:
            score += 2
        elif new_price <= 90:
            score += 1.5
        else:
            score += 1

        return score

    def get_products_detail(self, file_name):
        score = 0
        # seclect all products
        try:
            self.driver.find_element(
                by=By.XPATH, value="//*[text()='Voir tous les produits']").click()
            time.sleep(3)
        except Exception as e:
            print(e)
        # tri per % de reduction
        try:
            tri = self.driver.find_element(by=By.ID, value="mdr_tri")
            tri.select_by_value('promo_sort_desc')
            time.sleep(3)
        except Exception as e:
            print(e)
        self.scroll_down()
        self.scroll_up()
        # grab products
        for i in range(1, 6):
            try:
                products = self.driver.find_elements(
                    by=By.CLASS_NAME, value="l-categorie__bloc_produit.bloc_product.bloc_product_"+str(i))
                for product in products:
                    dispo = product.find_element(
                        by=By.CLASS_NAME, value="mt-1.txt-center.txt-s").text
                    if "Epuisé" == dispo:
                        continue
                    else:
                        vid = product.get_attribute("data-vid")
                        pid = product.get_attribute("data-pid")
                        try:
                            shop = product.find_element(
                                by=By.CLASS_NAME, value="nom_vente--item").text
                        except Exception as e:
                            print(e)
                            shop = "........."
                        name = product.find_element(
                            by=By.CLASS_NAME, value="bloc_produit__item-nom.txt-center.eventClick").text
                        price_original = product.find_element(
                            by=By.CLASS_NAME, value="ml-2.txt-crossed.txt-muted").text
                        price_new = product.find_element(
                            by=By.CLASS_NAME, value="fw-extra-bold.txt-l.txt-primary").text
                        link = product.find_element(
                            by=By.CLASS_NAME, value="checkDisclaimer.eventClick").get_attribute("href").split("&")
                        price_new = price_new.replace(' €', '')
                        price_new = price_new.replace('\n', ' ')
                        price_new = price_new.replace(',', '.')
                        price_new = price_new.replace(' ', '')
                        price_new = price_new.replace('Àpartirde', '')
                        price_new = price_new.replace('àpartirde', '')

                        price_original = price_original.replace(' €', '')
                        price_original = price_original.replace(' ', '')
                        price_original = price_original.replace(',', '.')
                        if "partir" not in price_new:
                            discount = 100 - (float(price_new) /
                                              float(price_original)) * 100
                            score = self.get_score(discount, price_new)
                            # print ('___________', score)
                        else:
                            discount = 0
                            score = 0

                        image = "https://archive.showroomprive.com/v2/images_content_split/" + \
                            vid+"/products_"+pid+"_image1_medium.jpg"
                        file_name.write(
                            f'<td>{shop}</td><td><a href={link[0]}>{name}</a> </td><td>{price_new} €</td><td>{price_original} €</td><td>{round(discount,3)} %</td><td>{score}</td><td><img src="{image}??width=613&heigh=250&quality=60"> </td></tr><tr>\n')
            except Exception as e:
                print(e)

    def get_product_detail(self, file_name):
        score = 0
        self.scroll_down()
        self.scroll_up()
        # grab products
        products = self.driver.find_elements(
            by=By.CLASS_NAME, value="l-categorie__bloc_produit.bloc_product")
        try:
            products += self.driver.find_elements(
                by=By.CLASS_NAME, value="l-categorie__bloc_produit.bloc_product.bloc_product")
            for product in products:
                dispo = product.find_element(
                    by=By.CLASS_NAME, value="mt-1.txt-center.txt-s").text
                if "Epuisé" == dispo:
                    continue
                else:
                    vid = product.get_attribute("data-vid").replace(" ", "")
                    pid = product.get_attribute("data-pid").replace(" ", "")
                    try:
                        shop = product.find_element(
                            by=By.CLASS_NAME, value="nom_vente--item").text
                    except Exception as e:
                        shop = vid
                    # name = product.find_element(
                        # by=By.XPATH, value="//title").text.split('/')[0].replace("Vente", "")
                    name = product.find_element(
                        by=By.CLASS_NAME, value="bloc_produit__item-nom.txt-center").text
                    price_original = product.find_element(
                        by=By.CLASS_NAME, value="ml-2.txt-crossed.txt-muted").text
                    price_new = product.find_element(
                        by=By.CLASS_NAME, value="fw-extra-bold.txt-l.txt-primary").text
                    link = product.find_element(
                        by=By.CLASS_NAME, value="eventClick").get_attribute("href").split("&")
                    price_new = price_new.replace(' €', '')
                    price_new = price_new.replace('\n', ' ')
                    price_new = price_new.replace(',', '.')
                    price_new = price_new.replace(' ', '')
                    price_new = price_new.replace('Àpartirde', '')

                    price_original = price_original.replace(' €', '')
                    price_original = price_original.replace(' ', '')
                    price_original = price_original.replace(',', '.')
                    if "partir" not in price_new:
                        discount = 100 - (float(price_new) /
                                          float(price_original)) * 100
                        score = self.get_score(discount, price_new)
                        # print ('___________', score)
                    else:
                        discount = 0
                        score = 0

                    image = "https://cedex.it.showroomprive.com/v2/images_content_split/" + \
                        vid+"/products_"+pid+"_image1_medium.jpg"
                    file_name.write(
                        f'<td>{shop}</td><td><a href={link[0]}>{name}</a> </td><td>{price_new} €</td><td>{price_original} €</td><td>{round(discount,3)} %</td><td>{score}</td><td><img src="{image}??width=613&heigh=250&quality=60"> </td></tr><tr>\n')
        except Exception as e:
            print(e)

    def get_visited_shops(self):
        with open(JSON_VISITED_SHOPS, "r") as json_file:
            shops = json.load(json_file)
        self.visited_shops_links = shops["show_room_FR"]

    def search(self, tag, max_price=30, gender="M", order_by="asc_price"):
        self.driver.get(
            'https://www.showroomprive.com/resultatrecherche.aspx?recherche='+tag)
        order_by_select = Select(
            self.driver.find_element(by=By.ID, value='mdr_tri'))
        time.sleep(1)
        order_by_select.select_by_index(1)

    def get_all_categories(self):
        self.nav_list = []
        self.nav_list_links = []
        nav_list_element = self.driver.find_element(by=By.XPATH, value="//nav")
        items = nav_list_element.find_elements_by_tag_name("li")
        for item in items:
            sale_category = item.text
            sale_category_link = self.driver.find_elements_by_partial_link_text(
                sale_category)
            sale_category_href = sale_category_link[0].get_attribute("href")
            if sale_category_href[-1] == "0" or sale_category_href[-1] == "5" or sale_category_href[-1] == "9":
                continue
            self.nav_list.append(sale_category)
            self.nav_list_links.append(sale_category_href)

    def get_all_shops(self):
        self.shops_names = []
        self.shops_links = []
        self.new_visited_shops = []
        for i in range(len(self.nav_list_links)):
            self.driver.get(self.nav_list_links[i])
            self.scroll_down()
            self.scroll_up()
            for item in self.driver.find_elements_by_class_name("sale-block"):
                if (None != item.get_attribute("data-salename")) and (item.get_attribute("data-saleid") not in self.shops_links) and ("vente_avenir" not in item.get_attribute("id")):
                    self.new_visited_shops.append(
                        item.get_attribute("data-saleid"))
                    if (item.get_attribute("data-saleid") not in self.visited_shops_links):
                        self.shops_names.append(
                            item.get_attribute("data-salename"))
                        self.shops_links.append(
                            item.get_attribute("data-saleid"))
        print(self.shops_names)
        print(self.shops_links)

    def set_visited_shops(self):
        with open(JSON_VISITED_SHOPS, "r") as jsonFile:
            data = json.load(jsonFile)
        data["show_room_FR"] = self.new_visited_shops
        with open(JSON_VISITED_SHOPS, "w") as jsonFile:
            json.dump(data, jsonFile)

    def get_all_products(self):
        wb = load_workbook(XLSX_TEMPLATE)
        for i in range(len(self.shops_links)):
            self.driver.get(ALL_PRODUCTS+self.shops_links[i])
            # self.driver.get("https://www.showroomprive.com/tousproduits.aspx?vente=63060")
            self.scroll_down()
            self.scroll_up()
            for item in self.driver.find_elements_by_class_name('bloc_product'):
                lDispo = True
                sheet = ""
                lScore = 0
                lDecote = ""
                lName = ""
                lOriginalPrice = "0"
                if ("isponible" not in (item.find_element_by_class_name('bloc_produit__item-dispo').text)):
                    lDispo = False
                if lDispo:
                    """try:
                        image_item = item.find_element_by_tag_name("img")
                        image_link = image_item.get_attribute("src")
                        print ('https:/'+image_link)
                        img = openpyxl.drawing.image.Image('https:/'+image_link)
                    except:
                        img = openpyxl.drawing.image.Image("files/default.png")"""
                    try:
                        lDecote = item.find_element_by_class_name(
                            'bloc_produit__item-decote').text
                    except:
                        lDecote = "-0%"
                    try:
                        lName = item.find_element_by_class_name(
                            'bloc_produit__item-nom').text
                    except:
                        lName = ""
                    try:
                        lOriginalPrice = item.find_element_by_class_name(
                            'bloc_produit__item-prix_boutique').text
                    except:
                        lOriginalPrice = "0"
                    try:
                        lFinalPrice = item.find_element_by_class_name(
                            'bloc_produit__item-prix').text
                        lFinalPrice_1 = lFinalPrice[:lFinalPrice.find(',')]
                        lFinalPrice_1 = re.sub("\D", "", lFinalPrice_1)
                    except:
                        lFinalPrice_1 = "0"
                    if (0 <= int(lFinalPrice_1) <= 10):
                        sheet = "0--10"
                    elif (10 < int(lFinalPrice_1) <= 20):
                        sheet = "10--20"
                    elif (20 < int(lFinalPrice_1) <= 50):
                        sheet = "20--50"
                    elif (50 < int(lFinalPrice_1) <= 100):
                        sheet = "50--100"
                    else:
                        sheet = ">>100"
                    try:
                        lScore = int(lDecote[1:-1]) + int(lFinalPrice_1)
                    except:
                        lScore = 50
                    # insert product in xlx file
                    wb[sheet].append([
                        self.shops_names[i],
                        lName,
                        lOriginalPrice,
                        lDecote,
                        lFinalPrice,
                        lScore,
                        "https://www.showroomprive.com/ficheproduit.aspx?produit="+item.get_attribute("data-pid")]
                    )
                    # img.anchor(wb[sheet].cell(row="H", column=wb[sheet]._current_column))
            wb.save(self.timestr + '_ShowRoomPrive_FR.xlsx')

    def scroll_down(self):
        # Get scroll height
        last_height = self.driver.execute_script(
            "return document.body.scrollHeight")
        while True:
            # Scroll down to bottom
            self.driver.execute_script(
                "window.scrollTo(0, document.body.scrollHeight);")
            # Wait to load page
            time.sleep(SCROLL_PAUSE_TIME)
            # Calculate new scroll height and compare with last scroll height
            new_height = self.driver.execute_script(
                "return document.body.scrollHeight")
            if new_height == last_height:
                break
            last_height = new_height

    def scroll_up(self):
        # Scroll up to bottom
        time.sleep(SCROLL_PAUSE_TIME)
        self.driver.execute_script(
            "window.scrollTo( document.body.scrollHeight, 0);")

    def get_product_info(link):
        pass

    def get_sell_id():
        id = 60862
        return id

    def insert_product(self, sheet, original_price, final_price, name, category="", shop="",  discount="",  score="", image_link=""):
        sheet.append(sheet.nrows, 0, category)
        sheet.append(sheet.nrows, 1, shop)
        sheet.append(sheet.nrows, 2, name)
        sheet.append(sheet.nrows, 3, original_price)
        sheet.append(sheet.nrows, 4, discount)
        sheet.append(sheet.nrows, 5, final_price)
        sheet.append(sheet.nrows, 6, score)
        sheet.append(sheet.nrows, 7, image_link)

    def creatXLSX(self):
        # Create an new Excel file and add a worksheet.
        self.timestr = time.strftime("%Y-%m-%d-%H:%M:%S")
        self.workbook = xlsxwriter.Workbook(self.timestr+'.xlsx')
        # Add a bold format to use to highlight cells.
        bold = self.workbook.add_format({'bold': True})
        self.worksheet1 = self.workbook.add_worksheet("0--10")
        self.worksheet2 = self.workbook.add_worksheet("10--20")
        self.worksheet3 = self.workbook.add_worksheet("20--50")
        self.worksheet4 = self.workbook.add_worksheet("50--100")
        self.worksheet5 = self.workbook.add_worksheet(">>100")
        for worksheet in [self.worksheet1, self.worksheet2, self.worksheet3, self.worksheet4, self.worksheet5]:
            # Widen the first column to make the text clearer.
            worksheet.set_column('A:A', 15)
            worksheet.set_column('B:B', 15)
            worksheet.set_column('C:C', 30)
            worksheet.set_column('D:D', 12.25)
            worksheet.set_column('F:F', 12.25)
            # worksheet.write('A2', 'Category', bold)
            worksheet.write('A2', 'Shop', bold)
            worksheet.write('B2', 'Product', bold)
            worksheet.write('C2', 'Original_Price', bold)
            worksheet.write('D2', 'Discount', bold)
            worksheet.write('E2', 'Final_price', bold)
            worksheet.write('F2', 'Score', bold)
            worksheet.write('G2', 'link', bold)
            worksheet.write('H2', 'Image', bold)
        # Insert an image.
        # worksheet1.insert_image('B5', 'logo.png')
        self.workbook.close()


if __name__ == "__main__":
    showRoom_FR = ShowRoomPrive_FR()
    showRoom_FR.login()
    # showRoom_FR.get_outlet_80()
    # showRoom_FR.get_permenant_shops()
    showRoom_FR.get_specific_shops()
    # showRoom_FR.get_outlet_alloffer()
    # showRoom_FR.get_all_mode_products()
    # showRoom_FR.get_visited_shops()
    # showRoom_FR.get_all_categories()
    # showRoom_FR.get_all_shops()
    # showRoom_FR.set_visited_shops()
    # showRoom_FR.get_all_products()
    showRoom_FR.driver.quit()
