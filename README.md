# ShowroomPrive Crawler

**showroomprive-crawler** project is a Python script that utilizes both Scrapy and Selenium to crawl the Showroomprive website and scrape information on available products and their prices. The scraped data is then saved to an HTML page, allowing for easy visualization and analysis.

This project can be particularly useful for businesses or individuals interested in gathering data on products and prices from the Showroomprive website, such as market researchers, e-commerce retailers, or price comparison websites. By utilizing both Scrapy and Selenium, the showroomprive-crawler can effectively navigate through the site and extract relevant data, while also providing a user-friendly output format in the form of an HTML page.

## Requirements installing

```bash
pip3 install -r requirements.txt
```
## Running 🏃
To run the Crawler 
```bash
python3 showRoomPrive_FR.py
```

## Results

![image](img/screenshots/01.png))

